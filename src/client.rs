use std::env;
use gitlab::Gitlab;

const GITLAB_TOKEN_ENV: &str = "GITLAB_ACCESS_TOKEN";
const GITLAB_URL_ENV: &str = "GITLAB_URL";

pub fn new() -> Gitlab {
    let h = expect_env(GITLAB_URL_ENV);
    let t = expect_env(GITLAB_TOKEN_ENV);

    Gitlab::new(h, t).unwrap()
}

fn expect_env(k: &str) -> String {
    return env::var(k).expect(format!("${k} is expected.").as_str());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn env() {
        expect_env(GITLAB_URL_ENV);
        expect_env(GITLAB_TOKEN_ENV);
    }

    #[test]
    fn client() {
        new();
    }
}
