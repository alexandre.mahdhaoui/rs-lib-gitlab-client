# rs-lib-gitlab-client

## Overview

`rs_lib_gitlab_client` is a cargo library used to easily generate a Gitlab Client.

## Usage

Expects 2 environment variables:
- `GITLAB_ACCESS_TOKEN` 
- `GITLAB_URL`


```rust
let client = rs_lib_gitlab_client::new()
```
